from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from gallery.models import Image, Like
from gallery.forms import ImageForm, RegisterForm


def index(request):
    images_list = Image.objects.order_by('-popularity')
    context = {'images_list': images_list}
    return render(request, 'gallery/index.html', context)

def my_images(request):
    my_images_list = Image.objects.filter(owner=request.user.id).order_by('-date')
    return render(request, 'gallery/my_images.html', {'my_images_list': my_images_list})

def details(request, image_id):
    image = Image.objects.get(pk=image_id)
    liked = False
    image.save()
    if Like.objects.all():
        if Like.objects.filter(image_id=image.id, user_id=request.user.id).exists():
            liked = True
    return render(request, 'gallery/details.html', {'image': image, 'liked': liked})

def like_image(request, image_id):
    l = Like(image_id = image_id, user_id = request.user.id)
    l.save()
    image = Image.objects.get(pk = image_id)
    image.popularity += 1
    image.save()
    return redirect('gallery:details', image_id = image_id)

def unlike_image(request, image_id):
    if Like.objects.filter(image_id = image_id, user_id = request.user.id).exists():
        l = Like.objects.get(image_id = image_id, user_id = request.user.id)
        l.delete()
        image = Image.objects.get(pk = image_id)
        image.popularity -= 1
        image.save()
    return redirect('gallery:details', image_id = image_id)

def liked(request):
    likes_list = Like.objects.filter(user_id=request.user.id)
    images_list = []
    for like in likes_list:
        images_list.append(Image.objects.get(pk=like.image_id))
    return render(request, 'gallery/liked.html', {'images_list': images_list})
    
def add(request):
    submitted = False
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            image = form.save(commit=False)
            image.owner = request.user.id
            image.save()
            return HttpResponseRedirect('/gallery/add?submitted=True')
    else:
        form = ImageForm()
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'gallery/add.html', {'form':form, 'submitted':submitted})

def signup(request):
    error_text = ''
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password1 = form.cleaned_data['password1']
            user = authenticate(username=username, password=password1)   
            login(request, user)
            messages.success(request, ("Utworzono konto!"))   
            return redirect('gallery:index') 
    else:
        form = RegisterForm()        
    return render(request, 'gallery/signup.html', {'form': form, 'error_text': error_text})

def signin(request):
    if request.method == 'POST':
        username = request.POST.get('login')
        password1 = request.POST.get('password')
        user = authenticate(username=username, password=password1)
        if user is not None:
            login(request, user)
            return redirect('gallery:index')
        else:
            messages.error(request, "Niepoprawne dane")
    return render(request, 'gallery/signin.html')

def signout(request):
    logout(request)
    return redirect('/gallery/signin/')

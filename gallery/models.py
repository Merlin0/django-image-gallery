from datetime import date
from django.contrib.auth.models import User
from django.utils.timezone import now

from django.db import models

class Image(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1024, null=True, blank=True)
    date = models.DateTimeField(default=now)
    photo = models.ImageField(null=True, blank=True, upload_to='images/')
    popularity = models.IntegerField(default=0)
    owner = models.IntegerField(blank=False, default=1)

    def __str__(self):
        return self.title
    
class Like(models.Model):
    image_id = models.IntegerField(blank=False, default=1)
    user_id = models.IntegerField(blank=False, default=1)

    def __str__(self):
        return "user{0}_likes_image{1}".format(self.user_id, self.image_id)
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'gallery'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:image_id>/', views.details, name='details'),
    path('add/', views.add, name='add'),
    path('signin/', views.signin, name='signin'),
    path('signup/', views.signup, name='signup'),
    path('signout/', views.signout, name='signout'),
    path('my_images/', views.my_images, name='my_images'),
    path('<int:image_id>/like_image/', views.like_image, name='like_image'),
    path('<int:image_id>/unlike_image/', views.unlike_image, name='unlike_image'),
    path('liked/', views.liked, name='liked'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

from django import forms
from django.forms import ModelForm, ValidationError
from .models import Image
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'description', 'photo']
        labels = {
            'title': 'Tytuł',
            'description': 'Opis',
            'photo': 'Zdjęcie'
        }
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-input'}),
            'description': forms.Textarea(attrs={'class':'form-input'})
        }


class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder':'Login'})
        self.fields['email'].widget.attrs.update({'placeholder':'Email'})
        self.fields['password1'].widget.attrs.update({'placeholder':'Hasło'})
        self.fields['password2'].widget.attrs.update({'placeholder':'Potwierdź hasło'})

        for fieldname in ['username', 'email', 'password1', 'password2']:
            self.fields[fieldname].help_text = None
            self.fields[fieldname].label = ''

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("An user with this email already exists!")
        return email
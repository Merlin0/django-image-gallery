
from unittest.mock import MagicMock
from django.test import TestCase, Client
from django.urls import reverse
from gallery.models import Image, User, Like
from django.conf import settings
import os
from factory import Faker
import factory
from django.core.files.uploadedfile import SimpleUploadedFile

class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image
    
    title = 'test_title'
    description = 'test_description'
    popularity = Faker('random_int', min=0, max=100)
    owner = 1

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        mock_file = MagicMock(spec=SimpleUploadedFile, name='test_image.jpg', content=b'test_image_data', content_type='image/jpeg')
        kwargs['photo'] = mock_file
        return super()._create(model_class, *args, **kwargs)


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username = "test_user", email = "test@example.com", password = "Testcase1")
        self.client.force_login(user=self.user)

        self.temp_user = User.objects.create_user(username = "temp_user", email = "temp@example.com", password = "tempTestcase2")
        ImageFactory(owner = self.temp_user.id)
        ImageFactory()
        ImageFactory()
       
        # Image.objects.create(title = 'test1', description = 'test1', photo = SimpleUploadedFile(name='test_image1.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg'), popularity = 0)
        # Image.objects.create(title = 'test2', description = 'test2', photo = SimpleUploadedFile(name='test_image2.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg'), popularity = 5)
        
        # self.temp_user = User.objects.create_user(username = "temp_user", email = "temp@example.com", password = "tempTestcase2")
        # Image.objects.create(title = 'test3', description = 'test3', photo = SimpleUploadedFile(name='test_image3.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg'), owner = self.temp_user.id)

        self.images = Image.objects.all()
        self.test_image = Image.objects.latest('id')

        image_path = os.path.join(settings.MEDIA_ROOT, 'images/4.jpg')
        self.data = {'title': 'Test', 'description': 'Test', 'photo': SimpleUploadedFile(name='test.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')}
        self.new_user = {'username': 'TestUser', 'email': 'testuser@example.com', 'password1': 'Case123user', 'password2': 'Case123user'}
        self.login_user = {'login': 'test_user', 'password': 'Testcase1'}

        self.index_url = reverse('gallery:index')
        self.details_url = reverse('gallery:details', args=[self.test_image.id])
        self.my_images_url = reverse('gallery:my_images')
        self.like_image_url = reverse('gallery:like_image', args=[self.test_image.id])
        self.unlike_image_url = reverse('gallery:unlike_image', args=[self.test_image.id])
        self.liked_url = reverse('gallery:liked')
        self.add_url = reverse('gallery:add')
        self.signup_url = reverse('gallery:signup')
        self.signin_url = reverse('gallery:signin')
        self.signout_url = reverse('gallery:signout')


    # Test index view
    def test_gallery_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)

    def test_gallery_index_without_images(self):
        response = self.client.get(self.index_url)
        self.assertEqual(response.status_code, 200)

    # Test details view
    def test_gallery_details_existing_image_without_like(self):
        response = self.client.get(self.details_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['liked'], False)
        self.assertContains(response, f'href="{reverse("gallery:like_image", args=[self.test_image.id])}"')


    def test_gallery_details_existing_image_with_like(self):
        Like.objects.create(image_id=self.test_image.id, user_id=self.user.id)
        response = self.client.get(self.details_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['liked'], True)
        self.assertContains(response, f'href="{reverse("gallery:unlike_image", args=[self.test_image.id])}"')


    def test_gallery_details_not_existing_image(self):
        test_id = self.test_image.id + 1
        with self.assertRaises(Image.DoesNotExist):
            self.client.get(reverse('gallery:details', args=[test_id]))


    def test_gallery_details_user_not_authenticated(self):
        self.client.logout()
        response = self.client.get(self.details_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['liked'], False)
        self.assertContains(response, f'href="{reverse("gallery:signin")}"')

    # Test my_images view
    def test_gallery_my_images_user_authenticated(self):
        response = self.client.get(self.my_images_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['my_images_list']), 2) # Tylko dwa obrazy są przypisane do zalogowanego użytkownika (test3 - owner = self.temp_user.id)
        latest_image = Image.objects.latest('date')
        #self.assertEqual(response.context['my_images_list'][0].id, latest_image.id) # Pierwszym obrazem powinnien być ostatni dodany (order_by('-date'))


    def test_gallery_my_images_user_not_authenticated(self):
        self.client.logout()
        response = self.client.get(self.my_images_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['my_images_list']), 0)
        self.assertContains(response, "Zaloguj się aby przeglądać dodane przez siebie zdjęcia")

    # Test like_image view
    def test_gallery_like_image_popularity_check(self):
        pre_popularity = self.test_image.popularity
        self.client.post(self.like_image_url)
        updated_image = Image.objects.get(pk = self.test_image.id)
        self.assertEqual(len(Like.objects.filter(image_id = self.test_image.id, user_id = self.user.id)), 1)
        self.assertEqual(updated_image.popularity, pre_popularity + 1)


    def test_gallery_like_image_like_object(self):
        self.client.post(self.like_image_url)
        self.assertEqual(len(Like.objects.filter(image_id = self.test_image.id, user_id = self.user.id)), 1)

    
    def test_gallery_like_image_redirect(self):
        response = self.client.post(self.like_image_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, f'/gallery/{self.test_image.id}/')

    # Test unlike_image view
    def test_gallery_unlike_image_popularity_check(self):
        Like.objects.create(image_id = self.test_image.id, user_id = self.user.id)
        pre_popularity = self.test_image.popularity
        self.client.post(self.unlike_image_url)
        updated_image = Image.objects.get(pk = self.test_image.id)
        self.assertEqual(len(Like.objects.filter(image_id = self.test_image.id, user_id = self.user.id)), 0)
        self.assertEqual(updated_image.popularity, pre_popularity - 1)


    def test_gallery_unlike_image_like_object(self):
        Like.objects.create(image_id = self.test_image.id, user_id = self.user.id)
        self.client.post(self.unlike_image_url)
        self.assertEqual(len(Like.objects.filter(image_id = self.test_image.id, user_id = self.user.id)), 0)

    
    def test_gallery_unlike_image_redirect(self):
        response = self.client.post(self.unlike_image_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, f'/gallery/{self.test_image.id}/')


    def test_gallery_unlike_image_not_existing_like(self):
        self.client.post(self.unlike_image_url)
        self.assertEqual(len(Like.objects.filter(image_id = self.test_image.id, user_id = self.user.id)), 0)

    # Test liked view
    def test_gallery_liked_user_authenticated_with_liked_images(self):
        Like.objects.create(image_id=self.test_image.id, user_id=self.user.id)
        response = self.client.get(self.liked_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['images_list']), 1)


    def test_gallery_liked_user_authenticated_without_liked_images(self):
        response = self.client.get(self.liked_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['images_list']), 0)
    

    def test_gallery_liked_user_not_authenticated(self):
        self.client.logout()
        response = self.client.get(self.liked_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['images_list']), 0) 
        self.assertContains(response, "Zaloguj się aby przeglądać ulubione zdjęcia")

    # Test add view
    def test_gallery_add_POST_valid_data(self):
        response = self.client.post(self.add_url, self.data, format='multipart')
        self.assertEqual(response.status_code, 302)
        self.assertIn(response.url, '/gallery/add?submitted=True')
        self.assertTrue(response.url.endswith('?submitted=True'))


    def test_gallery_add_GET(self):
        response = self.client.get(self.add_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['submitted'], False)


    def test_gallery_add_POST_invalid_data(self):
        self.data['title'] = ''
        response = self.client.post(self.add_url, self.data, format='multipart')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['submitted'], False)


    def test_gallery_add_user_not_authenticated(self):
        self.client.logout()
        response = self.client.get(self.add_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Zaloguj się aby dodać zdjęcie")

    # Test signup view
    def test_gallery_signup_POST_valid_data(self):
        response = self.client.post(self.signup_url, self.new_user)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/gallery/')


    def test_gallery_signup_POST_in_valid_data(self):
        self.new_user['username'] = ''
        response = self.client.post(self.signup_url, self.new_user)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())


    def test_gallery_signup_POST_creates_user(self):
        self.client.post(self.signup_url, self.new_user)
        self.assertTrue(User.objects.filter(username = 'TestUser').exists())


    def test_gallery_signup_GET(self):
        response = self.client.get(self.signup_url, self.new_user)
        self.assertEqual(response.status_code, 200)
    
    # Test signin view
    def test_gallery_signin_POST_valid_data(self):
        response = self.client.post(self.signin_url, self.login_user)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/gallery/')


    def test_gallery_signin_POST_in_valid_data(self):
        self.login_user['login'] = ''
        response = self.client.post(self.signin_url, self.login_user)
        self.assertEqual(response.status_code, 200)


    def test_gallery_signin_view_GET(self):
        response = self.client.get(self.signin_url)
        self.assertEqual(response.status_code, 200)

    # Test signout view
    def test_gallery_signout_redirect_to_signin(self):
        response = self.client.get(self.signout_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/gallery/signin/')


    def tearDown(self):
        for image in self.images:
            image.photo.delete()


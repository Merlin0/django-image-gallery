import django
import django.test
from django.urls import reverse, resolve
from gallery.forms import ImageForm, RegisterForm
import os
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from gallery.models import User


class TestForms(django.test.TestCase):       

    def test_image_form_valid_data(self):
        # Arrange
        image_path = os.path.join(settings.MEDIA_ROOT, 'images/4.jpg')
        data = {
            'title': 'image1',
            'description': 'description1',
            'photo': SimpleUploadedFile(name='test_image.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg'),
        }
        # Act
        image_form = ImageForm(data)
        # Assert
        self.assertTrue(image_form.is_valid())


    def test_image_form_no_data(self):
        data = {}

        image_form = ImageForm(data)

        self.assertFalse(image_form.is_valid())
        for field in image_form.errors:
            self.assertEqual(image_form.errors[field], ['This field is required.'])


    def test_image_form_no_description(self):
        image_path = os.path.join(settings.MEDIA_ROOT, 'images/4.jpg')
        data = {
            'title': 'image1',
            'description': None,
            'photo': SimpleUploadedFile(name='test_image.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg'),
        }

        image_form = ImageForm(data)

        self.assertTrue(image_form.is_valid())


    def test_register_form_valid_data(self):
        data = {
            'username': 'test',
            'email': 'test@gmail.com',
            'password1': 'testcase1',
            'password2': 'testcase1',
        }

        register_form = RegisterForm(data)

        self.assertTrue(register_form.is_valid())


    def test_register_form_no_data(self):
        data = {}

        register_form = RegisterForm(data)

        self.assertFalse(register_form.is_valid())
        for field in register_form.errors:
            self.assertEqual(register_form.errors[field], ['This field is required.'])


    def test_register_form_different_passwords(self):
        data = {
            'username': 'test',
            'email': 'test@gmail.com',
            'password1': 'testcase1',
            'password2': 'testcase2',
        }

        register_form = RegisterForm(data)

        self.assertFalse(register_form.is_valid())
        self.assertEqual(register_form.errors['password2'], ['The two password fields didn’t match.'])

    
    def test_register_form_username_already_used(self):
        User.objects.create_user(username = "test_user", email = "test@example.com", password = "Testcase1")
        data = {
            'username': 'test_user',
            'email': 'test@gmail.com',
            'password1': 'testcase1',
            'password2': 'testcase1',
        } 

        register_form = RegisterForm(data)

        self.assertFalse(register_form.is_valid())
        self.assertEqual(register_form.errors['username'], ['A user with that username already exists.'])


    def test_register_form_email_already_used(self):
        User.objects.create_user(username = "test_user", email = "test@example.com", password = "Testcase1")
        data = {
            'username': 'admin',
            'email': 'test@example.com',
            'password1': 'testcase1',
            'password2': 'testcase1',
        } 

        register_form = RegisterForm(data)

        self.assertFalse(register_form.is_valid())
        self.assertEqual(register_form.errors['email'], ['An user with this email already exists!'])


    def test_register_form_incorrect_password(self):
        data = {
            'username': 'admin',
            'email': 'test@example.com',
            'password1': 'test',
            'password2': 'test',
        } 

        register_form = RegisterForm(data)
        import ipdb; ipdb.set_trace()

        self.assertFalse(register_form.is_valid())
        self.assertEqual(register_form.errors['password2'], ['The password is too similar to the email address.', 'This password is too short. It must contain at least 8 characters.', 'This password is too common.'])
